import { ApolloServer, gql } from 'apollo-server-express';
import express from 'express';
import session from 'express-session';
import connectRedis from 'connect-redis';
import typeDefs from './typeDef';
import resolvers from './resolvers';
import config from './config/config'
import mongoose from 'mongoose';
import db from './config/database'

(async () => {

    try {

        await mongoose.connect(
            `mongodb://${db.DB_USERNAME}:${ encodeURIComponent(db.DB_PASSWORD) }@${db.DB_HOST}:${db.DB_PORT}/${db.DB_NAME}`,
            { useNewUrlParser: true }
        );
        
        const app = express();
        app.disable('x-powered-by');
            
        const RedisStore = connectRedis( session );

        const store = new RedisStore({
            host: config.redis.redisHost,
            port: config.redis.redisPassword,
            pass: config.redis.redisPassword
        });

        app.use(session({
            store,
            name: config.session.sessName,
            secret: config.session.sessSecret,
            resave: false,
            saveUninitialized: false,
            cookie: {
                maxAge: config.session.sessLifetime,
                sameSite: true,
                secure: config.server.mode === 'production'
            }
        }));
        
        const server = new ApolloServer({
            // These will be defined for both new or existing servers
            typeDefs,
            resolvers,
            playground: config.server.mode === 'development',
            context: ({ req, res }) => ({ req, res })
        });
        
        server.applyMiddleware({ app }); // app is from an existing express app
        
        app.listen({ port: config.server.port }, () =>
        console.log(`🚀 Server ready at http://localhost:4000${server.graphqlPath}`)
        )
    } catch (error) {
        console.error(error);
    }

}) ();