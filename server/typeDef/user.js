import { gql } from 'apollo-server-express';

// GraphQL Schema
export default gql`
    extend type Query {
        me: Users
        user(id: ID!): Users
        users: [Users!]!
    }

    extend type Users {
        id: ID!
        firstname: String,
        middlename: String,
        lastname: String,
        username: String,
        email: String!,
        password: String!,
        gender: Int!
        createdAt: String!
        updatedAt: String!
    }

    extend type Mutation {
        signUp(firstname: String, middlename: String, lastname: String,
            username: String, email: String!, password: String!, gender: Int!): Users
        signIn(email: String!, password: String!): Users
        signOut: Boolean
    }
`;