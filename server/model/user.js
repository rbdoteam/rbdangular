import mongoose from "mongoose";
import { hash, compare } from "bcryptjs";

const userSchema = new mongoose.Schema({
    firstname: String,
    middlename: String,
    lastname: String,
    username: {
        type: String,
        validate: {
            validator: async username => await User.doesntExist({ username }),
            message: ({ value }) => `Username '${value}' has already been taken`
        }
    },
    email: {
        type: String,
        validate: {
            validator: async email => await User.doesntExist({ email }),
            message: ({ value }) => `Email ${value} has already been in use`
        }
    },
    password: String,
    gender: Number,
}, 
{
    timestamps: true
});

userSchema.pre('save', async function() {
    if(this.isModified('password')) {
        this.password = await hash(this.password, 10);
    }
});

userSchema.statics.doesntExist = async function (options) {
    return await this.where(options).countDocuments() === 0;
}

userSchema.methods.matchesPassword = function (password) {
    return compare(password, this.password);
}

const User = mongoose.model('Users', userSchema);
export default User;