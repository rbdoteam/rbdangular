import { AuthenticationError } from 'apollo-server-express';
import { User } from './model';
import config from './config/config';

export const attemptSignin  = async (email, password) => {
    const msg = 'Incorrect email or password! Please try again';
    const user = await User.findOne({email});
    if ( !user ) {
        throw new AuthenticationError(msg);
    }

    if ( !await user.matchesPassword(password) ) {
        throw new AuthenticationError(msg);
    }

    return user;
}

const isSignedIn = req => req.session.userId;
export const checkSignedIn = req => {
    if( !isSignedIn( req ) ) {
        throw new AuthenticationError('You must be signed in');
    }
}

export const checkSignedOut = req => {
    if( isSignedIn( req ) ) {
        throw new AuthenticationError('You are already signed in');
    }
}

export const signOut = (req, res) => new Promise((resolve, reject) => {
    req.session.destroy(err => {
        if(err) reject(err);

        res.clearCookie(config.session.sessName);
        resolve(true);
    })
})