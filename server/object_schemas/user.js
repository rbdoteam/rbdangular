import Joi from "joi";

const firstname = Joi.string().label('First Name').max(30);
const middlename = Joi.string().label('Middle Name').max(30);
const lastname = Joi.string().label('Last Name').max(30);
const username = Joi.string().alphanum().label('Username').min(4).max(30);
const email = Joi.string().email().label('Email').required();
const password = Joi.string().min(8).max(50).regex(/^(?=\S*[a-z])(?=\S*[A-Z])(?=\S*\d).*$/).label('Password').required().options({
    language: {
        string: {
            regex: {
                base: 'Password must have one lowercase letter, one uppercase letter, one digit'
            }
        }
    }
});
const gender = Joi.number().required().label('Gender');

export const signUp = Joi.object().keys({
    firstname, middlename, lastname, username,
    email, password, gender

});

export const signIn = Joi.object().keys({
    email, password

});