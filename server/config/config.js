require('dotenv').config();

const { MODE, PORT, SESS_NAME, SESS_SECRET, REDIS_HOST, REDIS_PORT, REDIS_PASSWORD } = process.env
const config = {
    server : {
        mode: MODE,
        port: PORT
    },
    session: {
        sessName: SESS_NAME,
        sessSecret: SESS_SECRET,
        sessLifetime: 1000 * 60 * 60 * 2
    },
    redis: {
        redisHost: REDIS_HOST,
        redisPort: REDIS_PORT,
        redisPassword: REDIS_PASSWORD
    }
};

export default config;