import mongoose from 'mongoose';
import { UserInputError } from "apollo-server-express";
import { User } from "../model";
import Joi from 'joi';
import { signUp, signIn } from '../object_schemas';
import * as Auth from '../auth';

export default {
    Query: {
        me: function ( root, args, {req}, info ) {
            Auth.checkSignedIn( req );

            return User.findById(req.session.userId);
        },
        users: function(root, args, {req}, info) {
            Auth.checkSignedIn( req );
            return User.find({}); 
        },
        user: function (root, {id}, {req}, info) {
            if ( ! mongoose.Types.ObjectId.isValid(id) ) {
                throw new UserInputError(`${id} is not a valid user ID!`);
            }
    
            Auth.checkSignedIn( req );
            return User.findById(id);
        }
    },
    Mutation: {
        signUp: async function (root, args, {req}, info) {

            checkSignedOut( req );
            await Joi.validate(args, signUp, {abortEarly: false});

            const user = await User.create(args);
            req.session.userId = user.id
            
            return user;
        },
        signIn: async function ( root, args, {req}, info ) {
            const { userId } = req.session;
            if ( userId )
                return User.findById(userId);

            await Joi.validate( args, signIn, { abortEarly: false } )
            
            const user = await Auth.attemptSignin( args.email, args.password );

            req.session.userId = user.id
            return user;
        },
        signOut: function ( root, args, {req, res}, info ) {
            Auth.checkSignedIn( req );
            return Auth.signOut(req, res);
        }
    }
};